# OpenShift intro with Node.js

## Making the v2 image

Since Node.js moves pretty quickly, upgrading to a new language runtime could be
a relevant activity to show, instead of just changing the server's response.
Update the `microdnf module enable` command in the Containerfile from `12` to `14`.
