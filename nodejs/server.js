const express = require('express')
const app = express()
const port = 8000

process.on('SIGINT', () => process.exit(1))

app.get('/', (req, res) => {
  res.send('Hello World!\n')
})

app.listen(port, () => {
  console.log(`Example app listening at port ${port}`)
})
