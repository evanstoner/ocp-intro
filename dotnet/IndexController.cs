using Microsoft.AspNetCore.Mvc;

namespace dotnet {
  public class IndexController : Controller {
    [Route("")]
    public string Index() {
      return "Hello World!\n";
    }
  }
}
