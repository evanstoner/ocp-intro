# OpenShift intro with .NET 5.0

## Local development

You don't need to do this to run the demo since everything is containerized. But
if you want to work on the project outside of a container:

```shell
sudo dnf install dotnet-sdk-5.0
```

This project was created with `dotnet new webapi` and then removing as much as
possible.

## Relevant files

`IndexController.cs` - Returns "Hello World!" request.

## Base images

See Containerfile for two images (multi-stage build).
