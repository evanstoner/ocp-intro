import http.server
import socketserver
from http import HTTPStatus


class Handler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(HTTPStatus.OK)
        self.end_headers()
        self.wfile.write(b'Hello World!\n')


port = 8000
httpd = socketserver.TCPServer(('', port), Handler)
print("Listening on port %i" % port)
httpd.serve_forever()