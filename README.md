# OpenShift Container Platform introductions for developers

Walk through day-in-the-life developer activities with language-specific examples.

## One time setup

**Create quay.io repository.** For simplicity just create one repo, `ocp-intro`,
which you can reuse between languages. Create a robot account that has write
access to this repository.

**Provision an OCP environment.** Red Hatters can use the RHPDS shared environment.

**Have a Linux box with Podman and Cockpit installed.**
Consider using Fedora if RHEL 8.5 is not yet released, to take advantage of
a performance improvement in the overlay storage system: https://www.redhat.com/sysadmin/podman-rootless-overlay. This improves the speed of `podman build`.

## Script

### Setup

Consult the README.md in the language directory for any specific setup.

In Linux:

1. `podman login quay.io`
1. In the language directory, `podman build .` to sanity check and also pull base images.

In OpenShift:

1. Create a project for the live work (e.g. dev), and a project for the Pipeline demo (e.g. stage).
1. Install Red Hat OpenShift Pipelines operator.
1. In the stage project:
   1. Create a Secret for the Quay.io robot and link it to the `pipeline`
      ServiceAccount. This is required to push the pipeline-created image. Note that
      you must use basic-auth (not dockerconfigjson) type if using OCP <4.7 (which is
      currently the case in the RHPDS shared environment).
   1. Create resources from `.pipeline/` directory.
   1. Launch Pipeline with `ocp-intro-pvc`.

In the browser, open these tabs:

- OpenShift developer console
- quay.io ocp-intro repo
- Cockpit terminal in the language directory
- GitLab project language directory

### Building and running our app locally

```shell
# Review application code and Containerfile.
echo "Inspect the relevant file for the language."
vi Containerfile

# Build a v1 container image with podman.
podman build --tag quay.io/estoner/ocp-intro:v1 .

# Run the v1 image with podman.
podman images | head -2
podman run --detach --publish 8000:8000 --name ocp-intro quay.io/estoner/ocp-intro:v1
podman ps
curl localhost:8000

# Build and push a v2 container image (notice the speed using the cache).
echo "Options: Modify the Containerfile to select a different language runtime version."
echo "     or: Modify the source code to print 'Hello World v2!'"
podman build --tag quay.io/estoner/ocp-intro:v2 .
podman images | head -3

# Run the v2 image side by side with the v1 image
podman run --detach --publish 8001:8000 --name ocp-intro-v2 quay.io/estoner/ocp-intro:v2
podman ps
curl localhost:8001
curl localhost:8000

# Push the container images to quay.io, a public registry.
podman push quay.io/estoner/ocp-intro:v1
podman push quay.io/estoner/ocp-intro:v2
```

### Deploying our app to OpenShift

1. Create a Deployment object for v1 of our app.

Topology view > Add container image > quay.io/estoner/ocp-intro:v1

Select Deployment to watch Pods status go from Container Creating to Running.

2. Review scaling and resilience.

Click deployment circle > Details > scale up to 2, then 3.

Resources > select a pod > Review metrics, logs, and terminal

Actions > Delete Pod

Topology > deployment circle > Details

3. Deploy v2 in another project to show isolation

Project > Create Project > staging

Add > Container images > quay.io/estoner/ocp-intro:v2

Open URL

> (as of 11/30/22 this doesn't work, see [ODC-7203](https://issues.redhat.com/browse/ODC-7203))
> 3. Upgrade our app with the latest version.
>
> Explain rolling upgrade
>
> Actions > Edit ocp-intro > Change to v2 > Details

### Exposing our app

Create a Service and Route, and view the resulting URL.

Deployment > Resources > review Service and Route

### Automating build and deploy

Review YAML

Switch projects, discuss isolation

Show PipelineRun

Revisit deployed application

### Cleanup

OpenShift: Delete Application (the ring around the Deployment).

Quay.io: Delete v1 and v2 tags.

```shell
# RHEL: stop containers and remove images
podman rm -f -t 0 ocp-intro ocp-intro-v2
podman rmi quay.io/estoner/ocp-intro:v1 quay.io/estoner/ocp-intro:v2
```